/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.ActivoFijo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Bryan
 */
@Local
public interface ActivoFijoFacadeLocal {

    void create(ActivoFijo activoFijo);

    void edit(ActivoFijo activoFijo);

    void remove(ActivoFijo activoFijo);

    ActivoFijo find(Object id);

    List<ActivoFijo> findAll();

    List<ActivoFijo> findRange(int[] range);

    int count();
    
}
