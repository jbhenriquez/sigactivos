/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.ActivoFijo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Bryan
 */
@Stateless
public class ActivoFijoFacade extends AbstractFacade<ActivoFijo> implements ActivoFijoFacadeLocal {

    @PersistenceContext(unitName = "SigActivosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActivoFijoFacade() {
        super(ActivoFijo.class);
    }
    
}
