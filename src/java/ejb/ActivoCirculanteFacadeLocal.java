/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.ActivoCirculante;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Bryan
 */
@Local
public interface ActivoCirculanteFacadeLocal {

    void create(ActivoCirculante activoCirculante);

    void edit(ActivoCirculante activoCirculante);

    void remove(ActivoCirculante activoCirculante);

    ActivoCirculante find(Object id);

    List<ActivoCirculante> findAll();

    List<ActivoCirculante> findRange(int[] range);

    int count();
    
}
