/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.ActivoCirculante;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Bryan
 */
@Stateless
public class ActivoCirculanteFacade extends AbstractFacade<ActivoCirculante> implements ActivoCirculanteFacadeLocal {

    @PersistenceContext(unitName = "SigActivosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActivoCirculanteFacade() {
        super(ActivoCirculante.class);
    }
    
}
