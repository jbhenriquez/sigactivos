//Entity que representa la tabla Usuario
package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * SigActivos
 * 22/10/23
 * @author Bryan Henriquez
 * HA22023
 * PRN315 FIA UES
 */
@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable{
    
    @Id
    @Column(name = "ID_USUARIO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @NotNull
    @Size(min = 1, max=50)
    @Column(name = "USERNAME")
    private String usuario;
    
    @NotNull(message="Ingrese una contraseña")
    @Size(min=3,max=16, message="La contraseña debe tener min=3 y max=16 caracteres")
    @Column(name="CONTRASENA")
    private String contrasena;
    
    
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ROL", referencedColumnName = "ID_ROL")
    private Rol rol;
    
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_EMPLEADO", referencedColumnName = "id_empleado")
    private Empleado empleado;

    
    @Column(name = "ACTIVO")
    private Boolean activo;
    
    //constructores
    public Usuario() {
        this.empleado = new Empleado();
        this.rol = new Rol();
    }

    public Usuario(int id, String usuario, String contrasena, Rol rol, Empleado empleado, Boolean activo) {
        this.id = id;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol = rol;
        this.empleado = empleado;
        this.activo = activo;
    }

    
    
    //getter y setter
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
    
    
    
}