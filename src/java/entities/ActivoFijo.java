package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * SigActivos
 * 22/10/23
 * @author Bryan Henriquez
 * HA22023
 * PRN315 FIA UES
 */
@Entity
@Table(name="ACTIVO")
public class ActivoFijo implements Serializable{
    
    //atributos
    @Id
    @Column(name="ID_ACTIVO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @NotNull
    @Size(min=1,max=100)
    @Column(name="DESCRIPCION")
    private String descripcion;
    
    @NotNull
    @Size(min=1,max=50)
    @Column(name="MARCA")
    private String marca;
    
    @Size(min=1,max=50)
    @Column(name="MODELO")
    private String modelo;
    
    @NotNull
    @Size(min=1,max=50)
    @Column(name="UBICACION")
    private String ubicacion;
    
    @NotNull
    @Column(name="PRECIO")
    private Double precio;
    
    
    @Column(name="OBSERVACIONES")
    private String observaciones;
    
    //constructores

    public ActivoFijo() {
    }

    public ActivoFijo(int id, String descripcion, String marca, String modelo, String ubicacion, String observaciones) {
        this.id = id;
        this.descripcion = descripcion;
        this.marca = marca;
        this.modelo = modelo;
        this.ubicacion = ubicacion;
        this.observaciones = observaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
    
    
    
}