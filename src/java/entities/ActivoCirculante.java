package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * SigActivos
 * 22/10/23
 * @author Bryan Henriquez
 * HA22023
 * PRN315 FIA UES
 */
@Entity
@Table(name="ACTIVO_CIRCULANTE")
public class ActivoCirculante implements Serializable{
    
    //atributos
    @Id
    @Column(name="ID_ACTIVO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @NotNull
    @Size(min=1,max=100)
    @Column(name="DESCRIPCION")
    private String descripcion;
    
    @NotNull
    @Column(name="VALOR")
    private Double valor;
    
    
    
    //constructores

    public ActivoCirculante() {
    }

    public ActivoCirculante(int id, String descripcion, Double valor) {
        this.id = id;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    
    
}