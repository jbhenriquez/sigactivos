package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * SigActivos
 * 22/10/23
 * @author Bryan Henriquez
 * HA22023
 * PRN315 FIA UES
 */
@Entity
@Table(name="Empleado")
public class Empleado implements Serializable{
    
    //atributos
    @Id
    @Column(name="id_empleado")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @NotNull
    @Size(min=1,max=50)
    @Column(name="nombre")
    private String nombre;
    
    @NotNull
    @Size(min=1,max=50)
    @Column(name="apellido")
    private String apellido;
    
    @Column(name="telefono")
    private int telefono;
    
    //constructores

    public Empleado() {
    }

    public Empleado(int id, String nombre, String apellido, int telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }
    
    //getter setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    

}