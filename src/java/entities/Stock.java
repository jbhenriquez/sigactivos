
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Bryan
 */
@Entity
@Table(name="STOCK")
public class Stock implements Serializable{
    
    @Id
    @Column(name="ID_STOCK")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "FECHA_MOD")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMod;
    
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_INVENTARIO", referencedColumnName = "ID_KEY")
    private Inventario inventario;
    
    @NotNull
    @Column(name="UBICACION")
    private String ubicacion;
    
    @NotNull
    @Column(name="PRECIO")
    private Double precio;

    @NotNull
    @Column(name="CANTIDAD")
    private Double cantidad;
    
    
    //Constructores
    
    public Stock() {
        this.inventario = new Inventario();
    }

    public Stock(int id, Date fechaMod, Inventario inventario, String ubicacion, Double precio, Double cantidad) {
        this.id = id;
        this.fechaMod = fechaMod;
        this.inventario = inventario;
        this.ubicacion = ubicacion;
        this.precio = precio;
        this.cantidad = cantidad;
    }
    
    //getters y setters

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }


    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }
    
    
}
