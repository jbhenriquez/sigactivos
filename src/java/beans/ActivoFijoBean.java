package beans;

import ejb.ActivoFijoFacadeLocal;
import entities.ActivoFijo;

import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class ActivoFijoBean {
    @Inject
    private ActivoFijoFacadeLocal activoFijoFacadeLocal;
    private ActivoFijo activoFijo;
    private List<ActivoFijo> activosFijos;
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        activoFijo = new ActivoFijo();
        
        // si estamos editando obtenemos e usuarioId
        String IdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("activoFijoId");
        if (IdParam != null){
            Integer afId = Integer.parseInt(IdParam);
            activoFijo = activoFijoFacadeLocal.find(afId);
        }
        
        this.activosFijos = activoFijoFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         activoFijoFacadeLocal.create(activoFijo);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         activoFijoFacadeLocal.edit(activoFijo);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
         activoFijoFacadeLocal.remove(activoFijo);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public ActivoFijo getActivoFijo() {
        return activoFijo;
    }

    public List<ActivoFijo> getActivosFijos() {
        return activosFijos;
    }

    
    
       
    
}

