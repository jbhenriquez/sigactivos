package beans;

import ejb.EmpleadoFacadeLocal;
import entities.Empleado;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class EmpleadoBean {
    @Inject
    private EmpleadoFacadeLocal empleadoFacadeLocal;
    private Empleado empleado;
    private List<Empleado> empleados;
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        empleado = new Empleado();
        
        // si estamos editando obtenemos e usuarioId
        String empleadoIdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("empleadoId");
        if (empleadoIdParam != null){
            Integer empleadoId = Integer.parseInt(empleadoIdParam);
            empleado = empleadoFacadeLocal.find(empleadoId);
        }
        
        this.empleados = empleadoFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         empleadoFacadeLocal.create(empleado);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         empleadoFacadeLocal.edit(empleado);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
         empleadoFacadeLocal.remove(empleado);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public Empleado getEmpleado() {
        return empleado;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }
    
       
    
}

