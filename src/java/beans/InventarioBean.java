package beans;

import ejb.InventarioFacadeLocal;
import entities.Inventario;

import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class InventarioBean {
    @Inject
    private InventarioFacadeLocal inventarioFacadeLocal;
    private Inventario inventario;
    private List<Inventario> listado;
    
    @PostConstruct
    public void init(){
        //inicializamos el inventario
        inventario = new Inventario();
        
        // si estamos editando obtenemos e Id
        String IdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("inventarioId");
        if (IdParam != null){
            Integer inventarioId = Integer.parseInt(IdParam);
            inventario = inventarioFacadeLocal.find(inventarioId);
        }
        
        this.listado = inventarioFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         inventarioFacadeLocal.create(inventario);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         inventarioFacadeLocal.edit(inventario);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
         inventarioFacadeLocal.remove(inventario);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public Inventario getInventario() {
        return inventario;
    }

    public List<Inventario> getListado() {
        return listado;
    }

}

