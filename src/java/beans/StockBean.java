package beans;

import ejb.InventarioFacadeLocal;
import ejb.StockFacadeLocal;
import entities.Inventario;
import entities.Stock;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class StockBean {
    @Inject
    private StockFacadeLocal stockFacadeLocal;
    @Inject
    private InventarioFacadeLocal inventarioFacadeLocal;
    
    private Stock stock;
    
    private List<Stock> listaStock;
    private List<Inventario> listaInventario;
    
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        stock = new Stock();
        
        
        // si estamos editando obtenemos e usuarioId
        String stockIdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("stockId");
        if (stockIdParam != null){
            Integer stockId = Integer.parseInt(stockIdParam);
            stock = stockFacadeLocal.find(stockId);
        }
        
        this.listaStock = stockFacadeLocal.findAll();
        this.listaInventario = inventarioFacadeLocal.findAll();
        
    }
    
    public void create() throws IOException {
         stockFacadeLocal.create(stock);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         stockFacadeLocal.edit(stock);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
        stock = stockFacadeLocal.find(stock.getId());
        stockFacadeLocal.remove(stock);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public Stock traerStock(String id){
        Stock stock = stockFacadeLocal.find(id);
        return stock;
    }
    public Stock getStock() {
        return stock;
    }

    public List<Stock> getListaStock() {
        return listaStock;
    }

    public List<Inventario> getListaInventario() {
        return listaInventario;
    }
    
    
    
    
}
