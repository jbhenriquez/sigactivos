package beans;

import ejb.ActivoCirculanteFacadeLocal;
import entities.ActivoCirculante;

import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class ActivoCirculanteBean {
    @Inject
    private ActivoCirculanteFacadeLocal activoCirculanteFacadeLocal;
    private ActivoCirculante activoCirculante;
    private List<ActivoCirculante> activosCirculantes;
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        activoCirculante = new ActivoCirculante();
        
        // si estamos editando obtenemos e usuarioId
        String IdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("activoCirculanteId");
        if (IdParam != null){
            Integer afId = Integer.parseInt(IdParam);
            activoCirculante = activoCirculanteFacadeLocal.find(afId);
        }
        
        this.activosCirculantes = activoCirculanteFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         activoCirculanteFacadeLocal.create(activoCirculante);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         activoCirculanteFacadeLocal.edit(activoCirculante);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
         activoCirculanteFacadeLocal.remove(activoCirculante);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public ActivoCirculante getActivoCirculante() {
        return activoCirculante;
    }

    public List<ActivoCirculante> getActivosCirculantes() {
        return activosCirculantes;
    }

    
    
}

