package beans;

import ejb.RolFacadeLocal;
import entities.Rol;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class RolBean {
    @Inject
    private RolFacadeLocal rolFacadeLocal;
    private Rol rol;
    private List<Rol> roles;
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        rol = new Rol();
        
        // si estamos editando obtenemos e usuarioId
        String rolIdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("rolId");
        if (rolIdParam != null){
            Integer rolId = Integer.parseInt(rolIdParam);
            rol = rolFacadeLocal.find(rolId);
        }
        
        this.roles = rolFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         rolFacadeLocal.create(rol);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         rolFacadeLocal.edit(rol);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
         rolFacadeLocal.remove(rol);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }

    public Rol getRol() {
        return rol;
    }

    public List<Rol> getRoles() {
        return roles;
    }

      
    
}

