package beans;

import ejb.EmpleadoFacadeLocal;
import ejb.RolFacadeLocal;
import ejb.UsuarioFacadeLocal;
import entities.Empleado;
import entities.Rol;
import entities.Usuario;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Bryan
 */

@ManagedBean
public class UsuarioBean {
    @Inject
    private UsuarioFacadeLocal usuarioFacadeLocal;
    @Inject
    private EmpleadoFacadeLocal empleadoFacadeLocal;
    @Inject
    private RolFacadeLocal rolFacadeLocal;
    private Usuario usuario;
    
    private List<Usuario> usuarios;
    private List<Empleado> empleados;
    private List<Rol> roles;
    
    @PostConstruct
    public void init(){
        //inicializamos el usuario
        usuario = new Usuario();
        
        
        // si estamos editando obtenemos e usuarioId
        String usuarioIdParam = FacesContext.getCurrentInstance()
                .getExternalContext().getRequestParameterMap().get("usuarioId");
        if (usuarioIdParam != null){
            Integer usuarioId = Integer.parseInt(usuarioIdParam);
            usuario = usuarioFacadeLocal.find(usuarioId);
        }
        
        this.usuarios = usuarioFacadeLocal.findAll();
        this.empleados = empleadoFacadeLocal.findAll();
        this.roles = rolFacadeLocal.findAll();
    }
    
    public void create() throws IOException {
         usuarioFacadeLocal.create(usuario);
         FacesContext.getCurrentInstance().getExternalContext()
                 .getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro creado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void edit() throws IOException {
         usuarioFacadeLocal.edit(usuario);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro actualizado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    public void delete() throws IOException {
        usuario = usuarioFacadeLocal.find(usuario.getId());
        usuarioFacadeLocal.remove(usuario);
         FacesContext.getCurrentInstance()
                 .getExternalContext().getFlash().setKeepMessages(true);
         FacesContext.getCurrentInstance()
                 .addMessage(null, new FacesMessage("Registro eliminado."));
         FacesContext.getCurrentInstance()
                 .getExternalContext().redirect("index.xhtml");
     }
    
    
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public Usuario getUsuario() {
        return usuario;
    }  

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public List<Rol> getRoles() {
        return roles;
    }
    
    
    
}
